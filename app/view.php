<?php

class View {
    function render($json) {
        header('Content-Type: application/json');
        die(json_encode($json));
    }

    function abort($status, $message) {
        http_response_code($status);
        header('Content-Type: application/json');
        die(json_encode([
            'error' => [
                'status' => $status,
                'message' => $message,
            ]
        ]));
    }
}
