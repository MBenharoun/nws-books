<?php
class Router {
    function __construct($routes, $view) {
        $this->routes = $routes;
        $this->view = $view;
    }

    function exec() {
        $path = $_REQUEST['path'];
        if (empty($path)) {
            $this->view->abort(400, "Missing or empty 'path' parameter");
        }

        foreach($this->routes as $pattern => $resource) {
            if (preg_match($pattern, $path, $matches)) {
                array_shift($matches);
                $res = new $resource($this->view);
                $method = [$res, strtolower($_SERVER['REQUEST_METHOD'])];
                if (! is_callable($method)) {
                    $this->view->abort(405, "Method not allowed");
                } else {
                    call_user_func_array($method, $matches);
                }
            }
        }
        $this->view->abort(404, "Resource not found");
    }
}
