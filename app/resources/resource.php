<?php

class Resource {
    function __construct($view) {
        $this->view = $view;
    }

    function render($json) {
        $this->view->render($json);
    }

    function abort($status, $message) {
        $this->view->abort($status, $message);
    }
}
