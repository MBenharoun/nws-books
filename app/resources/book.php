<?php

require_once __DIR__ . '/resource.php';

class BookResource extends Resource {
    function get($id) {
        $this->render(['id' => $id]);

    }
}

class BookListResource extends Resource {
    function get() {
        $data = file_get_contents("data.json");
        $file = json_decode($data);

        $this->render([
            'data' => $file,
            'count' =>1,
            'total' => 0,
            'links' =>['self' => "/index.php?path=books"],
        ]);
    }
}
