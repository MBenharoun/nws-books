<?php
$data = file_get_contents("data.json");



require_once __DIR__ . '/view.php';
require_once __DIR__ . '/router.php';
require_once __DIR__ . '/resources/book.php';

$routes = [
    '@^books/(.+)$@' => 'BookResource',
    '@^books$@' => 'BookListResource',
];
$view = new View();
$router = new Router($routes, $view);
$router->exec();