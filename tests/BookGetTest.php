<?php

use GuzzleHttp\Client;

class BookGetTest extends PHPUnit\Framework\TestCase {
    public function testGet1() {
        $client = new Client();

        $bookid = "f88941f3-6d81-4aab-81ee-1bea8ba3972e";
        $title = "The Art of Computer Programming, Volume 1: Fundamental Algorithms";

        $res = $client->request('GET', "http://localhost/index.php?path=books/{$bookid}");
        $this->assertSame(200, $res->getStatusCode());

        $contentType = $res->getHeader('Content-Type')[0];
        $mimeType = explode(';', $contentType)[0];
        $this->assertSame('application/json', $mimeType);

        $body = $res->getBody();
        $json = json_decode($body);
        $this->assertNotNull($json);
        $this->assertObjectHasAttribute('id', $json);
        $this->assertSame($bookid, $json->id);
        $this->assertSame($title, $json->title);
    }

    public function testGet2() {
        $client = new Client();

        $bookid = "52b637ff-75c1-469d-8fee-91d2cebf0236";
        $title = "The C Programming Language";

        $res = $client->request('GET', "http://localhost/index.php?path=books/{$bookid}");
        $this->assertSame(200, $res->getStatusCode());

        $body = $res->getBody();
        $json = json_decode($body);
        $this->assertNotNull($json);
        $this->assertSame($title, $json->title);
        $this->assertCount(2, $json->authors);

        $this->assertNotNull($json->links);
        $this->assertNotNull($json->links->self);
        $this->assertSame("/index.php?path=books/{$bookid}", $json->links->self);
    }

    public function test404() {
        $client = new Client();

        $bookid = uniqid();

        $res = $client->request('GET', "http://localhost/index.php?path=books/{$bookid}", ['http_errors' => false]);
        $this->assertSame(404, $res->getStatusCode());
    }
}
