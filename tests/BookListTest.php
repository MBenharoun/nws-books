<?php

use GuzzleHttp\Client;

/*
Example query: http://localhost:8080/index.php?path=books&limit=1

Returned json:
{
  "count": 1,
  "total": 2,
  "data": [
    {
      "id": 1,
      "title": "The Art of Computer Programming, Volume 1: Fundamental Algorithms",
      "authors": [
        "Donald E. Knuth"
      ],
      "ISBN10": "0201038013",
      "ISBN13": "9780201038019",
      "links": {
        "self": "/index.php?path=books/1"
      }
    }
  ],
  "links": {
    "self": "/index.php?path=books&limit=1",
    "prev": null,
    "next": "/index.php?path=books&offset=1&limit=1"
  }
}
*/

class BookListTest extends PHPUnit\Framework\TestCase {
    public function testBookList() {
        $client = new Client();

        $res = $client->request('GET', "http://localhost/index.php?path=books");
        $this->assertSame(200, $res->getStatusCode());

        $contentType = $res->getHeader('Content-Type')[0];
        $mimeType = explode(';', $contentType)[0];
        $this->assertSame('application/json', $mimeType);

        $body = $res->getBody();
        $json = json_decode($body);
        $this->assertNotNull($json);
        $this->assertNotNull($json->data);
        $this->assertNotNull($json->count);
        $this->assertNotNull($json->total);
        $this->assertNotNull($json->links);
        $this->assertNotNull($json->links->self);

        $count = $json->count;
        $this->assertCount($count, $json->data);

        $this->assertGreaterThanOrEqual($json->total, $json->count);

        $this->assertSame('/index.php?path=books', $json->links->self);
    }

    public function testBookLinks() {
        $client = new Client();

        $res = $client->request('GET', "http://localhost/index.php?path=books");
        $this->assertSame(200, $res->getStatusCode());

        $body = $res->getBody();
        $json = json_decode($body);

        $book = $json->data[0];
        $this->assertNotNull($book->links);
        $this->assertNotNull($book->links->self);
        $this->assertSame("/index.php?path=books/{$book->id}", $book->links->self);
    }
}
